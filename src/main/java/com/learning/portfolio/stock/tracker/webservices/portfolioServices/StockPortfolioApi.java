package com.learning.portfolio.stock.tracker.webservices.portfolioServices;

import com.learning.portfolio.stock.tracker.dao.services.StockPortfolioService;
import com.learning.portfolio.stock.tracker.dao.services.UserDataServices;
import com.learning.portfolio.stock.tracker.entity.UserData;
import com.learning.portfolio.stock.tracker.model.UpdateWatchListInput;
import com.learning.portfolio.stock.tracker.model.WatchlistPerformanceResponse;
import com.learning.portfolio.stock.tracker.webservices.exceptionHandler.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.text.ParseException;

import static com.learning.portfolio.stock.tracker.webservices.ErrorMessages.USER_NOT_PRESENT;
import static java.lang.String.format;

@RestController
@RequestMapping("/Portfolio/Stock")
public class StockPortfolioApi {

    @Autowired
    private UserDataServices userDataServices;
    @Autowired
    private StockPortfolioService stockPortfolioService;

    @ResponseBody
    @Produces(MediaType.APPLICATION_JSON)
    @GetMapping(value = "/Performance")
    public ResponseEntity<WatchlistPerformanceResponse> getWatchlistPerformance(@RequestParam(value = "userName") String userName)
            throws BusinessException, ParseException {
        UserData userData = userDataServices.getUser(userName);
        if (userData == null) {
            buildErrorResponseWhenUserNotPresent(userName);
        }
        return new ResponseEntity<>(stockPortfolioService.buildWatchlistPerformance(userName), HttpStatus.OK);
    }

    @Consumes(MediaType.APPLICATION_JSON)
    @PostMapping
    public void addToWatchlist(@Valid @RequestBody UpdateWatchListInput input) throws BusinessException {
        String username = input.getUserName();
        UserData userData = userDataServices.getUser(username);
        if (userData == null) {
            buildErrorResponseWhenUserNotPresent(username);
        }
        stockPortfolioService.addToPortfolio(input);
    }


    @Consumes(MediaType.APPLICATION_JSON)
    @DeleteMapping
    public void sellStockFromWatchlist(@Valid @RequestBody UpdateWatchListInput input) throws BusinessException {
        String username = input.getUserName();
        UserData userData = userDataServices.getUser(username);
        if (userData == null) {
            buildErrorResponseWhenUserNotPresent(username);
        }
        stockPortfolioService.removeFromPortfolio(input);
    }

    private void buildErrorResponseWhenUserNotPresent(@RequestParam("userName") String userName) throws BusinessException {
        throw new BusinessException(USER_NOT_PRESENT.getErrorCause(),
                format(USER_NOT_PRESENT.getErrorDetails(), userName), HttpStatus.BAD_REQUEST);
    }
}
