package com.learning.portfolio.stock.tracker.webservices.management.services;

import com.learning.portfolio.stock.tracker.entity.Stock;
import com.learning.portfolio.stock.tracker.repository.StocksRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/stock")
public class StockTickerService {

    @Autowired
    private StocksRepository stocksRepository;

    @ResponseBody
    @Produces(MediaType.APPLICATION_JSON)
    @RequestMapping(value = "/symbols", method = RequestMethod.GET)
    public ResponseEntity<Iterable<String>> getStockSymbols() {
        List<Stock> stockList = stocksRepository.findAll();
        return new ResponseEntity<>(stockList.stream()
                .map(Stock::getSymbol)
                .collect(Collectors.toList()), HttpStatus.OK);
    }
}
