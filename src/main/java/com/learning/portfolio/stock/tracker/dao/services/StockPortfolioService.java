package com.learning.portfolio.stock.tracker.dao.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.learning.portfolio.stock.tracker.cache.CacheIntervals;
import com.learning.portfolio.stock.tracker.cache.CacheManager;
import com.learning.portfolio.stock.tracker.cache.CachedObject;
import com.learning.portfolio.stock.tracker.entity.*;
import com.learning.portfolio.stock.tracker.entity.Stock;
import com.learning.portfolio.stock.tracker.model.*;
import com.learning.portfolio.stock.tracker.repository.StockLedgerRepository;
import com.learning.portfolio.stock.tracker.repository.StockPortfolioRepository;
import com.learning.portfolio.stock.tracker.repository.StocksRepository;
import com.learning.portfolio.stock.tracker.repository.UserDataRepository;
import com.learning.portfolio.stock.tracker.webservices.exceptionHandler.BusinessException;
import org.decimal4j.util.DoubleRounder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.lang.String.format;

@Service
public class StockPortfolioService {

    @Autowired
    private StockPortfolioRepository stockPortfolioRepository;
    @Autowired
    private UserDataRepository userDataRepository;
    @Autowired
    private StockLedgerRepository stockLedgerRepository;
    @Autowired
    private StocksRepository stocksRepository;

    @Autowired
    private UserDataServices userDataServices;
    private static CacheManager stockCMPCache = CacheManager.getInstance();

    private static String ALPHAVANTAGE_URL = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=%s&apikey=BFKGVR233TIQBF2E";

    private void refreshWatchlist(UserData userData) throws ParseException {
        List<StockPortfolio> userWatchList = stockPortfolioRepository.findByUserId(userData.getId()).orElse(new ArrayList<>());
        for (StockPortfolio stockPortfolio : userWatchList) {
            String stockSymbol = stockPortfolio.getSymbol();
            Optional<CachedObject> cachedInstanceOfStock = stockCMPCache.get(stockSymbol);
            if (cachedInstanceOfStock.isPresent()) {
                StockQuote quote = (StockQuote) cachedInstanceOfStock.get().getValue();
                updateWatchlistWithLatestQuote(stockPortfolio, quote);
            } else {
                fetchLatestQuoteAndUpdateCache(stockPortfolio, stockSymbol);
            }
        }
    }

    public List<StockPortfolio> getStockPortfolio(String username){
        UserData data = userDataServices.getUser(username);
        try {
            refreshWatchlist(data);
        } catch (ParseException e) {
            //TODO log and handle
            e.printStackTrace();
        }
        return stockPortfolioRepository.findByUserId(data.getId()).orElse(new ArrayList<>());
    }

    @Transactional
    public void addToPortfolio(UpdateWatchListInput input){
        UserData data = userDataServices.getUser(input.getUserName());
        StockLedger ledgerEntry = mapToStocksTransactionLedgerForBuy(input.getStock(),data);
        stockLedgerRepository.save(ledgerEntry);
        addToPortfolio(input, data);
    }

    @Transactional
    public void removeFromPortfolio(UpdateWatchListInput input) throws BusinessException {
        UserData data = userDataServices.getUser(input.getUserName());
        StockLedger ledgerEntry = mapToStocksTransactionLedgerForSell(input.getStock(),data);
        stockLedgerRepository.save(ledgerEntry);
        removeFromPortfolio(input,data);
    }

    public WatchlistPerformanceResponse buildWatchlistPerformance(String userName) {
        WatchlistPerformanceResponse response = new WatchlistPerformanceResponse();
        UserData data = userDataServices.getUser(userName);
        response.setUserName(data.getUsername());
        try {
            refreshWatchlist(data);
        } catch (ParseException e) {
            //TODO log and handle
            e.printStackTrace();
        }
        List<StockPortfolio> userWatchList = stockPortfolioRepository.findByUserId(data.getId()).orElse(new ArrayList<>());
        double aggregatedBuyPrice = 0;
        double aggregatedCurrentPrice = 0;
        for (StockPortfolio stock : userWatchList) {
            StockData stockData = new StockData();
            stockData.setStockName(stock.getSymbol());
            stockData.setBuyPrice(DoubleRounder.round(stock.getBuyPrice(), 2));
            stockData.setCurrentPrice(stock.getCurrentPrice());
            stockData.setQuantity(stock.getQuantity());
            stockData.setNotes(stock.getNotes());
            double totalCurrentPrice = stock.getCurrentPrice() * stock.getQuantity();
            double totalBuyPrice = stock.getBuyPrice() * stock.getQuantity();
            aggregatedBuyPrice = aggregatedBuyPrice + totalBuyPrice;
            aggregatedCurrentPrice = aggregatedCurrentPrice + totalCurrentPrice;
            double pl = totalCurrentPrice - totalBuyPrice;
            stockData.setProfitLoss(DoubleRounder.round(pl, 2));
            double profitLossPercent = (pl * 100) / totalBuyPrice;
            stockData.setProfitLossPercent(DoubleRounder.round(profitLossPercent, 2));
            response.getStockPerformance().add(stockData);
        }
        response.setNetWorth(DoubleRounder.round(aggregatedCurrentPrice, 2));
        response.setTotalAmountInvested(DoubleRounder.round(aggregatedBuyPrice, 2));

        double aggregatedProfitLoss = aggregatedCurrentPrice - aggregatedBuyPrice;
        response.setOverallProfitLoss(DoubleRounder.round(aggregatedProfitLoss, 2));

        double profitLossPercent = (aggregatedProfitLoss * 100) / aggregatedBuyPrice;
        response.setProfitLossPercent(DoubleRounder.round(profitLossPercent, 2));
        return response;
    }

    private StockLedger mapToStocksTransactionLedgerForBuy(StockSingleTransactionData stockSingleTransactionData,UserData data) {
        StockLedger ledger = new StockLedger();
        ledger.setUser(data);
        ledger.setPrice(stockSingleTransactionData.getPrice());
        ledger.setDate(stockSingleTransactionData.getTransactionDate());
        ledger.setSymbol(stockSingleTransactionData.getStockName());
        ledger.setType(TransactionType.BUY);
        ledger.setNotes(stockSingleTransactionData.getNotes());
        ledger.setQuantity(stockSingleTransactionData.getQuantity());
        return ledger;
    }

    private StockLedger mapToStocksTransactionLedgerForSell(StockSingleTransactionData stockSingleTransactionData,UserData data) {
        StockLedger ledger = new StockLedger();
        ledger.setUser(data);
        ledger.setPrice(stockSingleTransactionData.getPrice());
        ledger.setDate(stockSingleTransactionData.getTransactionDate());
        ledger.setSymbol(stockSingleTransactionData.getStockName());
        ledger.setType(TransactionType.SELL);
        ledger.setNotes(stockSingleTransactionData.getNotes());
        ledger.setQuantity(stockSingleTransactionData.getQuantity());
        return ledger;
    }

    private void addToPortfolio(UpdateWatchListInput input,UserData userData) {
        StockSingleTransactionData inputStock = input.getStock();
        try {
            refreshWatchlist(userData);
        } catch (ParseException e) {
            //TODO log and handle
            e.printStackTrace();
        }
        Optional<StockPortfolio> watchlistExistingElement = userData.getStockPortfolios().stream()
                .filter(stockPortfolio -> stockPortfolio.getSymbol().equals(inputStock.getStockName()))
                .findFirst();
        if (watchlistExistingElement.isPresent()) {
            updateExistingStockInWatchlist(inputStock, watchlistExistingElement.get());
        } else {
            stockPortfolioRepository.save(prepareWatchlistElement(userData, inputStock));
        }
    }

    private void removeFromPortfolio(UpdateWatchListInput input, UserData userData) throws BusinessException {
        StockSingleTransactionData inputStock = input.getStock();
        Optional<StockPortfolio> stockToSell = userData.getStockPortfolios().stream()
                .filter(stockPortfolio -> stockPortfolio.getSymbol().equals(inputStock.getStockName()))
                .findFirst();
        if (stockToSell.isPresent()) {
            StockPortfolio elementToEdit = stockToSell.get();
            if (inputStock.getQuantity() < elementToEdit.getQuantity()) {
                int quantity = elementToEdit.getQuantity() - inputStock.getQuantity();
                elementToEdit.setQuantity(quantity);
                elementToEdit.setUpdatedOn(inputStock.getTransactionDate());
                stockPortfolioRepository.save(elementToEdit);
            } else if (inputStock.getQuantity() == elementToEdit.getQuantity()) {
                stockPortfolioRepository.deleteById(elementToEdit.getId());
            } else {
                throw new BusinessException("Invalid sell quantity", "You can sell only up to " + inputStock.getQuantity(), HttpStatus.BAD_REQUEST);
            }
        } else {
            throw new BusinessException("Invalid stock", "Stock " + inputStock.getQuantity() + " is not present in watchlist", HttpStatus.BAD_REQUEST);
        }
    }


    private void updateExistingStockInWatchlist(StockSingleTransactionData inputStock, StockPortfolio stockPortfolioElement) {
        int totalQuantity = stockPortfolioElement.getQuantity() + inputStock.getQuantity();
        double avgBuyPrice = ((stockPortfolioElement.getQuantity() * stockPortfolioElement.getBuyPrice())
                + (inputStock.getQuantity() * inputStock.getPrice())) / totalQuantity;
        StockPortfolio stockPortfolioRef = stockPortfolioRepository.getOne(stockPortfolioElement.getId());
        stockPortfolioRef.setQuantity(totalQuantity);
        stockPortfolioRef.setBuyPrice(avgBuyPrice);
        stockPortfolioRef.setUpdatedOn(inputStock.getTransactionDate());
        stockPortfolioRepository.save(stockPortfolioRef);
    }

    private StockPortfolio prepareWatchlistElement(UserData userData, StockSingleTransactionData stockSingleTransactionData) {
        StockPortfolio newStockPortfolioElement = new StockPortfolio();
        newStockPortfolioElement.setSymbol(stockSingleTransactionData.getStockName());
        newStockPortfolioElement.setNotes(stockSingleTransactionData.getNotes());
        newStockPortfolioElement.setUpdatedOn(stockSingleTransactionData.getTransactionDate());
        newStockPortfolioElement.setBuyPrice(stockSingleTransactionData.getPrice());
        newStockPortfolioElement.setUser(userData);
        newStockPortfolioElement.setQuantity(stockSingleTransactionData.getQuantity());
        return newStockPortfolioElement;
    }


    private void updateWatchlistWithLatestQuote(StockPortfolio stockPortfolio, StockQuote quote) throws ParseException {
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(quote.getDate());
        stockPortfolio.setCurrentPrice(Double.parseDouble(quote.getClose()));
        stockPortfolio.setUpdatedOn(date);
        stockPortfolioRepository.save(stockPortfolio);
    }

    private void fetchLatestQuoteAndUpdateCache(StockPortfolio stockPortfolio, String stock) throws ParseException {
        String stockSymbol = "NSE:" + stock;
        StockQuote quote = getLatestQuote(stockSymbol).get(0);
        CachedObject cachedObject = new CachedObject(stock, quote,
                CacheIntervals.FIFTEEN_MINUTE_INTERVAL);
        stockCMPCache.put(cachedObject);
        updateWatchlistWithLatestQuote(stockPortfolio, quote);
    }

    private List<StockQuote> getLatestQuote(String symbol) {
        RestTemplate restTemplate = new RestTemplate();
        String url = format(ALPHAVANTAGE_URL, symbol);
        String response = restTemplate.getForObject(url, String.class);
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = null;
        try {
            jsonNode = objectMapper.readValue(response, JsonNode.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JsonNode jsonNode2 = jsonNode.get("Time Series (Daily)");
        Iterator<Map.Entry<String, JsonNode>> jsonIterator = jsonNode2.fields();
        List<StockQuote> quoteList = new ArrayList<>();
        while (jsonIterator.hasNext()) {
            StockQuote quote = new StockQuote();
            Map.Entry<String, JsonNode> entry = jsonIterator.next();
            quote.setDate(entry.getKey());
            quote.setOpen(entry.getValue().findValue("1. open").asText());
            quote.setHigh(entry.getValue().findValue("2. high").asText());
            quote.setLow(entry.getValue().findValue("3. low").asText());
            quote.setClose(entry.getValue().findValue("4. close").asText());
            quoteList.add(quote);
        }
        return quoteList;
    }
}
