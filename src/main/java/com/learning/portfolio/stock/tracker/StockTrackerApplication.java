package com.learning.portfolio.stock.tracker;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableBatchProcessing
@SpringBootApplication
public class StockTrackerApplication {

	public static void main(String[] args) {
		SpringApplication.run(StockTrackerApplication.class, args);
	}

}
