package com.learning.portfolio.stock.tracker;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.learning.portfolio.stock.tracker.model.RandomQuote;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;

@RestController
public class HelloController {

    @RequestMapping("/")
    public String greetings() {
        return returnQuote();
    }

    private String returnQuote() {
        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.getForObject("https://www.alphavantage.co/query?function=" +
                "TIME_SERIES_DAILY&symbol=NSE:ADANIPORTS&apikey=BFKGVR233TIQBF2E", String.class);
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = null;
        try {
            jsonNode = objectMapper.readValue(response, JsonNode.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JsonNode jsonNode2 = jsonNode.get("Time Series (Daily)");
        Iterator<Map.Entry<String, JsonNode>> jsonIterator = jsonNode2.fields();
        while (jsonIterator.hasNext()) {
                Map.Entry<String, JsonNode> entry = jsonIterator.next();
                String date = entry.getKey();
                String open = entry.getValue().findValue("1. open").asText();
                String high = entry.getValue().findValue("2. high").asText();
                String low = entry.getValue().findValue("3. low").asText();
                String close = entry.getValue().findValue("4. close").asText();
                System.out.println("Values are " + date +" "+open+" "+high+" "+low+" "+close);
        }
        RandomQuote[] quote = restTemplate.
                getForObject("http://quotesondesign.com/wp-json/posts", RandomQuote[].class);
        return Arrays.stream(quote).findFirst().get().getContent();
    }
}
