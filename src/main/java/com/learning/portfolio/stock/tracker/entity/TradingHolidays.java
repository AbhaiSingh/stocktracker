package com.learning.portfolio.stock.tracker.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tradingholidays")
public class TradingHolidays {

    public TradingHolidays() {
    }

    @Id
    @Temporal(TemporalType.DATE)
    @Column(name = "DATE")
    @JsonProperty("Date")
    private Date date;

    @Column(name = "MARKET_SEGMENT")
    @JsonProperty("MarketSegment")
    private String marketSegment;

    @Column(name = "DESCRIPTION")
    @JsonProperty("DESCRIPTION")
    private String description;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMarketSegment() {
        return marketSegment;
    }

    public void setMarketSegment(String marketSegment) {
        this.marketSegment = marketSegment;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
