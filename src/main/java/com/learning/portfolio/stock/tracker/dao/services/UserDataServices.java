package com.learning.portfolio.stock.tracker.dao.services;

import com.learning.portfolio.stock.tracker.entity.UserData;
import com.learning.portfolio.stock.tracker.model.User;
import com.learning.portfolio.stock.tracker.repository.UserDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class UserDataServices {

    @Autowired
    UserDataRepository userDataRepository;

    @Transactional
    public void createUser(User userInput) {
        UserData user = new UserData();
        user.setUsername(userInput.getUserName());
        user.setFirstName(userInput.getFirstName());
        user.setSurname(userInput.getLastName());
        user.setEmail(userInput.getEmailID());
        userDataRepository.save(user);
    }

    public boolean isUsernamePresent(String username){
        return userDataRepository.findByUsername(username).isPresent();
    }

    public UserData getUser(String username){
        return userDataRepository.findByUsername(username).orElse(null);
    }

    @Transactional
    //TODO Test if cascade delete is working
    public void deleteUser(String username){
        Optional<UserData> userDataOptional = userDataRepository.findByUsername(username);
        if(userDataOptional.isPresent()){
            UserData data = userDataOptional.get();
            userDataRepository.delete(data);
        }
    }
}
