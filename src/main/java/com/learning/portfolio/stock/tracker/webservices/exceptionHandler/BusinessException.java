package com.learning.portfolio.stock.tracker.webservices.exceptionHandler;

import org.springframework.http.HttpStatus;

public class BusinessException extends Exception{

    private String errorCause;
    private String errorDetails;
    private HttpStatus httpStatusToBeThrown;

    public BusinessException(String errorCause, String errorDetails, HttpStatus httpStatusToBeThrown) {
        super(errorCause);
        this.errorCause = errorCause;
        this.errorDetails = errorDetails;
        this.httpStatusToBeThrown = httpStatusToBeThrown;
    }

    public String getErrorCause() {
        return errorCause;
    }

    public String getErrorDetails() {
        return errorDetails;
    }

    public HttpStatus getHttpStatusToBeThrown() {
        return httpStatusToBeThrown;
    }
}
