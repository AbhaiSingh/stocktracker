package com.learning.portfolio.stock.tracker.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "Stocks")
@Getter
@Setter
@NoArgsConstructor
public class Stock {

    @Column(name = "SYMBOL")
    private String symbol;
    @Column(name = "LAST_UPDATED")
    private Date timestamp;
    @Id
    @Column(name = "ISIN")
    private String isin;
}
