package com.learning.portfolio.stock.tracker.webservices.management.services;

import com.learning.portfolio.stock.tracker.entity.TradingHolidays;
import com.learning.portfolio.stock.tracker.repository.TradingHolidaysRepository;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/NSE")
public class FileDownloadServices {

    private static String BHAV_COPY_URL = "https://www.nseindia.com/ArchieveSearch?h_filetype=eqbhav&date=%s&section=EQ";
    private static String BHAV_COPY_URL2 = "https://archives.nseindia.com/content/historical/EQUITIES/2020/MAY/cm27MAY2020bhav.csv.zip";
    private static String NSE_DOMAIN = "https://www.nseindia.com";
    @Autowired
    TradingHolidaysRepository tradingHolidaysRepository;

    @Autowired
    RestTemplateBuilder restTemplateBuilder;

    //TODO remove response returned, configurable and refactor it
    @GetMapping(value = "/Bhavcopy")
    public ResponseEntity<String> downloadLatestBhavcopy() {
        String path = new File("").getAbsolutePath() + File.separator + "output" +
                File.separator + "bhavcopy" + File.separator + "bhavcopy.csv.zip";
        //TODO make it generic with error handling features
        RestTemplate template = restTemplateBuilder.build();
        String bhavcopyDate = getLatestWeekday();
        /*String endpoint = String.format(BHAV_COPY_URL, bhavcopyDate);*/
        String endpoint = BHAV_COPY_URL2;
        ResponseEntity<String> responseEntity = template.getForEntity(endpoint, String.class);
        String val = responseEntity.toString();
        /*int start = val.indexOf("href=");
        int end = val.indexOf(".zip");
        String res = val.substring(start + 5, end + 4);*/
        String downloadEndpoint = BHAV_COPY_URL2;
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));
        HttpEntity<String> entity = new HttpEntity<String>(httpHeaders);
        ResponseEntity<byte[]> response = template.exchange(downloadEndpoint, HttpMethod.GET, entity,
                byte[].class, "1");
        if (response.getStatusCode() == HttpStatus.OK) {
            try {
                File file = new File(path);
                if (file.getParentFile().exists()) {
                    FileUtils.cleanDirectory(file.getParentFile());
                } else {
                    file.getParentFile().mkdirs();
                }
                Files.write(Paths.get(path), response.getBody());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return ResponseEntity.ok(path);
    }

    @GetMapping(value = "/EquityInfo")
    public ResponseEntity<String> downloadAllEquityReport(){
        String path = new File("").getAbsolutePath() + File.separator + "output" + File.separator + "equity" + File.separator + "equity.zip";
        //TODO make it generic with error handling features
        RestTemplate template = restTemplateBuilder.build();
        String endpoint = "https://www.nseindia.com/content/nsccl/nsccl_ann19.zip";

        ResponseEntity<String> responseEntity = template.getForEntity(endpoint, String.class);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));
        HttpEntity<String> entity = new HttpEntity<String>(httpHeaders);
        ResponseEntity<byte[]> response = template.exchange(endpoint, HttpMethod.GET, entity, byte[].class, "1");
        if (response.getStatusCode() == HttpStatus.OK) {
            try {
                File file = new File(path);
                if (file.getParentFile().exists()) {
                    FileUtils.cleanDirectory(file.getParentFile());
                } else {
                    file.getParentFile().mkdirs();
                }
                Files.write(Paths.get(path), response.getBody());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return ResponseEntity.ok(path);
    }

    private String getLatestWeekday() {
        SimpleDateFormat sdfmt = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        currentDate = DateUtils.addDays(currentDate, -1);
        while (isDateWeekend(currentDate) || isDateHoliday(currentDate)) {
            currentDate = DateUtils.addDays(currentDate, -1);
        }
        return sdfmt.format(currentDate);
    }

    private boolean isDateHoliday(final Date date) {
        List<TradingHolidays> holidays = tradingHolidaysRepository.findAll();
        return holidays.stream()
                .anyMatch(tradingHolidays -> DateUtils.isSameDay(tradingHolidays.getDate(), date));
    }

    private boolean isDateWeekend(final Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int weekday = calendar.get(Calendar.DAY_OF_WEEK);
        return weekday == Calendar.SATURDAY || weekday == Calendar.SUNDAY;
    }
}
