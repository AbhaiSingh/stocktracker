package com.learning.portfolio.stock.tracker.model;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

public class UpdateWatchListInput {
    public UpdateWatchListInput() {
    }

    @NotEmpty(message = "userName cannot be empty/null")
    private String userName;

    @Valid
    private StockSingleTransactionData stock;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public StockSingleTransactionData getStock() {
        return stock;
    }

    public void setStock(StockSingleTransactionData stock) {
        this.stock = stock;
    }
}
