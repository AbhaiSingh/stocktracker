package com.learning.portfolio.stock.tracker.repository;

import com.learning.portfolio.stock.tracker.entity.StockPortfolio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RepositoryRestResource(collectionResourceRel = "StockPortfolio", path = "StockPortfolio")
@Component
public interface StockPortfolioRepository extends JpaRepository<StockPortfolio, UUID> {

    void deleteById(Long id);

    @Query(value = "Select * from stock_portfolio where user_id= ?1", nativeQuery = true)
    Optional<List<StockPortfolio>> findByUserId(UUID userId);
}
