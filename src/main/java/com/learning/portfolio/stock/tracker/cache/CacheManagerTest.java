package com.learning.portfolio.stock.tracker.cache;

import com.learning.portfolio.stock.tracker.model.StockData;

public class CacheManagerTest {

    public static void main (String a[]) throws InterruptedException {
        StockData stock1 = new StockData();
        stock1.setStockName("TataMotors");
        stock1.setBuyPrice(345.43);
        stock1.setQuantity(123);
        StockData stockData2 = new StockData();
        stockData2.setStockName("FCL");
        stockData2.setQuantity(2132);
        stockData2.setBuyPrice(21);
        CachedObject object1 = new CachedObject(stock1.getStockName(),stock1,100);
        CachedObject object2 = new CachedObject(stockData2.getStockName(),stockData2,10000);
        CacheManager cacheInstance = CacheManager.getInstance();
        cacheInstance.put(object1);
        cacheInstance.put(object2);

        Thread.sleep(6000);
        cacheInstance.get(stock1.getStockName());
        cacheInstance.put(object1);
        Thread.sleep(6000);
        cacheInstance.get(stockData2.getStockName());
    }

}
