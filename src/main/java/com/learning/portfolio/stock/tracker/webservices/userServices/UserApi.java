package com.learning.portfolio.stock.tracker.webservices.userServices;

import com.learning.portfolio.stock.tracker.dao.services.UserDataServices;
import com.learning.portfolio.stock.tracker.model.User;
import com.learning.portfolio.stock.tracker.webservices.exceptionHandler.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
public class UserApi {

    @Autowired
    private UserDataServices userDataServices;

    @PostMapping
    public ResponseEntity createUser(@Valid @RequestBody User userInput) throws BusinessException {
        if (!userDataServices.isUsernamePresent(userInput.getUserName())) {
            userDataServices.createUser(userInput);
            return new ResponseEntity(HttpStatus.CREATED);
        } else {
            throw new BusinessException("UserName is already taken",
                    userInput.getUserName() + " is already taken.", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping
    public ResponseEntity deleteUser(@RequestParam(value = "username") String username) throws BusinessException {
        if (userDataServices.isUsernamePresent(username)) {
            userDataServices.deleteUser(username);
            return new ResponseEntity(HttpStatus.ACCEPTED);
        } else {
            throw new BusinessException("username doesn't exist", username + " doesn't exist", HttpStatus.BAD_REQUEST);
        }
    }
}
