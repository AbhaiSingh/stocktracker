package com.learning.portfolio.stock.tracker.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Stock {

    @JsonProperty("Time Series (Daily)")
    private List<StockData> stockDataList;

}
