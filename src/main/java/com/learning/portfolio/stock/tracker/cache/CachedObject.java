package com.learning.portfolio.stock.tracker.cache;

import java.util.Calendar;
import java.util.Date;

public class CachedObject implements Cacheble {

    private Object identifier;
    private Date expirationDate;

    public Object getValue() {
        return value;
    }

    private Object value;

    public CachedObject(Object identifier, Object value, long ttl){
        this.identifier=identifier;
        this.value=value;
        initializeExpirationDate(ttl);
    }

    public CachedObject(Object identifier, Object value, CacheIntervals intervals){
        this.identifier=identifier;
        this.value=value;
        initializeExpirationDate(intervals.getTimeInMillis());
    }

    private void initializeExpirationDate(long ttl) {
        Calendar date = Calendar.getInstance();
        long currTime = date.getTimeInMillis();
        expirationDate = new Date(currTime+ttl);
    }

    @Override
    public Object getIdentifier() {
        return identifier;
    }

    @Override
    public boolean isExpired() {
        Date currDate = new Date();
        return expirationDate.compareTo(currDate) < 0;
    }
}
