package com.learning.portfolio.stock.tracker.model;

import java.util.ArrayList;
import java.util.List;

public class WatchlistPerformanceResponse {

    private String userName;
    private List<StockData> stockPerformance;
    private double totalAmountInvested;
    private double netWorth;
    private double overallProfitLoss;
    private double profitLossPercent;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<StockData> getStockPerformance() {
        if(null == stockPerformance){
            stockPerformance = new ArrayList<>();
            return stockPerformance;
        }
        return stockPerformance;
    }

    public void setStockPerformance(List<StockData> stockPerformance) {
        this.stockPerformance = stockPerformance;
    }

    public double getTotalAmountInvested() {
        return totalAmountInvested;
    }

    public void setTotalAmountInvested(double totalAmountInvested) {
        this.totalAmountInvested = totalAmountInvested;
    }

    public double getNetWorth() {
        return netWorth;
    }

    public void setNetWorth(double netWorth) {
        this.netWorth = netWorth;
    }

    public double getOverallProfitLoss() {
        return overallProfitLoss;
    }

    public void setOverallProfitLoss(double overallProfitLoss) {
        this.overallProfitLoss = overallProfitLoss;
    }

    public double getProfitLossPercent() {
        return profitLossPercent;
    }

    public void setProfitLossPercent(double profitLossPercent) {
        this.profitLossPercent = profitLossPercent;
    }

    @Override
    public String toString() {
        return "WatchlistPerformanceResponse{" +
                "userName='" + userName + '\'' +
                ", stockPerformance=" + stockPerformance +
                ", totalAmountInvested=" + totalAmountInvested +
                ", netWorth=" + netWorth +
                ", overallProfitLoss=" + overallProfitLoss +
                ", profitLossPercent=" + profitLossPercent +
                '}';
    }
}
