package com.learning.portfolio.stock.tracker.repository;

import com.learning.portfolio.stock.tracker.entity.TradingHolidays;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Component;

import java.util.Date;

@RepositoryRestResource(collectionResourceRel = "TradingHolidays", path = "TradingHolidays")
@Component
public interface TradingHolidaysRepository extends JpaRepository<TradingHolidays, Date> {
}
