package com.learning.portfolio.stock.tracker.batch;

import com.learning.portfolio.stock.tracker.entity.Stock;
import com.learning.portfolio.stock.tracker.entity.TradingHolidays;
import com.learning.portfolio.stock.tracker.repository.StocksRepository;
import com.learning.portfolio.stock.tracker.repository.TradingHolidaysRepository;
import com.learning.portfolio.stock.tracker.scraper.nse.DownloadSpreadsheet;
import com.learning.portfolio.stock.tracker.webservices.management.services.FileDownloadServices;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Configuration
public class BhavcopyBatchConfiguration {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    StocksRepository stocksRepository;

    @Autowired
    TradingHolidaysRepository tradingHolidaysRepository;

    @Autowired
    DownloadSpreadsheet downloadSpreadsheet;

    @Autowired
    FileDownloadServices fileDownloadServices;

    public static final String BHAVCOPY_DESTINATION_DIR = new File("").getAbsolutePath()
            + File.separator + "output" + File.separator + "bhavcopy";
    //TODO refactor
    @Bean
    public FlatFileItemReader<Stock> reader() {
        String fileName = fileDownloadServices.downloadLatestBhavcopy().getBody();
        if (StringUtils.isNotEmpty(fileName)) {
            downloadSpreadsheet.checkfile_extract(fileName,BHAVCOPY_DESTINATION_DIR);
        }
        FlatFileItemReader<Stock> csvReader = new FlatFileItemReader<>();
        csvReader.setName("BhavcopyReader");
        File[] files = new File(BHAVCOPY_DESTINATION_DIR).listFiles();
        csvReader.setResource(new FileSystemResource(files[0].getPath()));
        csvReader.setLinesToSkip(1);
        csvReader.setLineMapper(new DefaultLineMapper<Stock>() {
            {
                setLineTokenizer(new DelimitedLineTokenizer() {
                    {

                        setNames(new String[]{"SYMBOL", "SERIES", "OPEN", "HIGH", "LOW", "CLOSE", "LAST", "PREVCLOSE", "TOTTRDQTY",
                                "TOTTRDVAL", "TIMESTAMP", "TOTALTRADES", "ISIN", ""});
                    }
                });
                setFieldSetMapper(new BeanWrapperFieldSetMapper<Stock>() {{
                    setStrict(false);
                    setDistanceLimit(1);
                    setTargetType(Stock.class);
                }});
            }
        });
        return csvReader;
    }

    @Bean
    public RepositoryItemWriter<Stock> writer() {
        RepositoryItemWriter<Stock> repositoryItemWriter = new RepositoryItemWriter<>();
        repositoryItemWriter.setRepository(stocksRepository);
        repositoryItemWriter.setMethodName("save");
        return repositoryItemWriter;
    }

    private Date getLatestWeekday() {
        Date currentDate = new Date();
        currentDate = DateUtils.addDays(currentDate, -1);
        while (isDateWeekend(currentDate) || isDateHoliday(currentDate)) {
            currentDate = DateUtils.addDays(currentDate, 1);
        }
        return currentDate;
    }

    private boolean isDateHoliday(final Date date) {
        List<TradingHolidays> holidays = tradingHolidaysRepository.findAll();
        return holidays.stream()
                .anyMatch(tradingHolidays -> DateUtils.isSameDay(tradingHolidays.getDate(), date));
    }

    private boolean isDateWeekend(final Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int weekday = calendar.get(Calendar.DAY_OF_WEEK);
        if (weekday == Calendar.SATURDAY || weekday == Calendar.SUNDAY) {
            return true;
        }
        return false;
    }

    @Bean
    public Job importUserJob(Step step1) {
        return jobBuilderFactory.get("bhavcopyJob")
                .incrementer(new RunIdIncrementer())
                .flow(step1)
                .end()
                .build();
    }

    @Bean
    public Step step1(RepositoryItemWriter<Stock> writer) {
        return stepBuilderFactory.get("step1")
                .<Stock, Stock>chunk(50)
                .reader(reader())
                .writer(writer())
                .build();
    }
}
