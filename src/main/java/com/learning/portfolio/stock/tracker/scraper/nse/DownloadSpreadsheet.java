package com.learning.portfolio.stock.tracker.scraper.nse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@Configuration
public class DownloadSpreadsheet {

    private static final Logger logger = LoggerFactory.getLogger(DownloadSpreadsheet.class);

    public void checkfile_extract(String file_name, String destinationDirectory) {
        try {
            extractFiles(file_name, destinationDirectory);
        } catch (IOException e) {
            logger.error("Exception occurred during extraction of bhavcopy " + e.getMessage());
        }
    }

    public static void extractFiles(String zipfilepath, String destdir) throws IOException {
        byte[] buffer = new byte[1024];
        try (FileInputStream fs = new FileInputStream(zipfilepath);
             ZipInputStream zs = new ZipInputStream(fs)) {
            ZipEntry ze = zs.getNextEntry();
            while (ze != null) {
                String filename = ze.getName();
                File newfile = new File(destdir + File.separator + filename);
                new File(newfile.getParent()).mkdirs();
                FileOutputStream fos = new FileOutputStream(newfile);
                int len;
                while ((len = zs.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
                zs.closeEntry();
                ze = zs.getNextEntry();
            }
            zs.closeEntry();
        } catch (Exception e) {
            logger.error("Error occurred during extraction " + e.getMessage());
        }finally {
            new File(zipfilepath).delete();
        }
    }
}
