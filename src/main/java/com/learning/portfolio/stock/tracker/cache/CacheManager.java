package com.learning.portfolio.stock.tracker.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class CacheManager {

    private static Map<Object,CachedObject> cachedObjectMap;
    private static long sleepTimeMillis = 5000;
    private static final Logger logger = LoggerFactory.getLogger(CacheManager.class);
    private CacheManager(){}

    public static CacheManager getInstance(){
        CacheManager manager = new CacheManager();
        manager.initialize();
        return manager;
    }

    public static CacheManager getInstance(long refreshInterval){
        sleepTimeMillis = refreshInterval;
        CacheManager manager = new CacheManager();
        manager.initialize();
        return manager;
    }

    private static void initialize() {
        cachedObjectMap = new HashMap<>();
        try{
            Thread cleanUpThread = new Thread(() -> {

                try{
                    while(!cachedObjectMap.isEmpty()){
                        logger.info("Cleaning up cache");
                        cachedObjectMap.entrySet()
                                .stream()
                                .filter(objectCachedObjectEntry -> objectCachedObjectEntry.getValue().isExpired())
                                .map(objectCachedObjectEntry -> objectCachedObjectEntry.getKey())
                                .collect(Collectors.toSet())
                                .stream()
                                .forEach(o -> cachedObjectMap.remove(o));
                        logger.info("Cache cleaned");
                    Thread.sleep(sleepTimeMillis);
                    }
                } catch (InterruptedException e) {
                    logger.error("Error occurred "+e);
                }
            });
            cleanUpThread.setPriority(Thread.MIN_PRIORITY);
            cleanUpThread.start();
        }catch (Exception e){
            logger.error("Error occurred in cache manager "+e);
        }
    }

    public Optional<CachedObject> get(Object identifier){
        return Optional.ofNullable(cachedObjectMap.get(identifier));
    }

    public void put(CachedObject object){
        cachedObjectMap.put(object.getIdentifier(),object);
    }

    public void clearCache(){
        cachedObjectMap.clear();
    }


}
