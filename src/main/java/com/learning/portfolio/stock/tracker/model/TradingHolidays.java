package com.learning.portfolio.stock.tracker.model;

import java.util.Date;

public class TradingHolidays {

    public TradingHolidays() {
    }
    private Date date;
    private String marketSegment;
    private String description;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMarketSegment() {
        return marketSegment;
    }

    public void setMarketSegment(String marketSegment) {
        this.marketSegment = marketSegment;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
