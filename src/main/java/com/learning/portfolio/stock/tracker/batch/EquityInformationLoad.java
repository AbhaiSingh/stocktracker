/*
package com.learning.portfolio.stock.tracker.batch;

import com.learning.portfolio.stock.tracker.entity.Stock;
import com.learning.portfolio.stock.tracker.entity.TradingHolidays;
import com.learning.portfolio.stock.tracker.repository.StocksRepository;
import com.learning.portfolio.stock.tracker.repository.TradingHolidaysRepository;
import com.learning.portfolio.stock.tracker.scraper.nse.DownloadSpreadsheet;
import com.learning.portfolio.stock.tracker.webservices.management.services.FileDownloadServices;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Configuration
public class EquityInformationLoad {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    StocksRepository stocksRepository;

    @Autowired
    DownloadSpreadsheet downloadSpreadsheet;

    @Autowired
    FileDownloadServices fileDownloadServices;

    public static final String EQUITY_DESTINATION_DIR = new File("").getAbsolutePath()
            + File.separator + "output" + File.separator + "equity";
    //TODO refactor
    @Bean
    public FlatFileItemReader<Stock> reader() {
        String fileName = fileDownloadServices.downloadAllEquityReport().getBody();
        if (StringUtils.isNotEmpty(fileName)) {
            downloadSpreadsheet.checkfile_extract(fileName,EQUITY_DESTINATION_DIR);
        }
        FlatFileItemReader<Stock> csvReader = new FlatFileItemReader<>();
        csvReader.setName("EquityReader");
        File[] files = new File(EQUITY_DESTINATION_DIR).listFiles();
        csvReader.setResource(new FileSystemResource(files[1].getPath()));
        csvReader.setLinesToSkip(1);
        csvReader.setLineMapper(new DefaultLineMapper<Stock>() {
            {
                setLineTokenizer(new DelimitedLineTokenizer() {
                    {

                        setNames(new String[]{"Sr. No.", "Symbol", "ISIN", "Security Name",
                                "Overall Permitted Qty. Across All Segments", "Memberwise Permitted Qty",
                                "Minimum Haircut %"});
                    }
                });
                setFieldSetMapper(new BeanWrapperFieldSetMapper<Stock>() {{
                    setStrict(false);
                    setDistanceLimit(1);
                    setTargetType(Stock.class);
                }});
            }
        });
        return csvReader;
    }

    @Bean
    public RepositoryItemWriter<Stock> writer() {
        RepositoryItemWriter<Stock> repositoryItemWriter = new RepositoryItemWriter<>();
        repositoryItemWriter.setRepository(stocksRepository);
        repositoryItemWriter.setMethodName("save");
        return repositoryItemWriter;
    }

    @Bean
    public Job importUserJob(Step step1) {
        return jobBuilderFactory.get("EquityInfoJob")
                .incrementer(new RunIdIncrementer())
                .flow(step1)
                .end()
                .build();
    }

    @Bean
    public Step step1(RepositoryItemWriter<Stock> writer) {
        return stepBuilderFactory.get("step1")
                .<Stock, Stock>chunk(50)
                .reader(reader())
                .writer(writer())
                .build();
    }

}
*/
