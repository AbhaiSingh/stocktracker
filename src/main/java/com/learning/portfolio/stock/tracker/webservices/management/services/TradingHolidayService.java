package com.learning.portfolio.stock.tracker.webservices.management.services;

import com.learning.portfolio.stock.tracker.entity.TradingHolidays;
import com.learning.portfolio.stock.tracker.repository.TradingHolidaysRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@RestController
@RequestMapping("/TradingHoliday")
public class TradingHolidayService {

    @Autowired
    private TradingHolidaysRepository tradingHolidaysRepository;

    @ResponseBody
    @Produces(MediaType.APPLICATION_JSON)
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Iterable<TradingHolidays>> getTradingHolidays() {
        return new ResponseEntity<>(tradingHolidaysRepository.findAll(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public void updateTradingHolidays(@RequestBody List<TradingHolidays> tradingHolidays){
        tradingHolidaysRepository.saveAll(tradingHolidays);
    }
}
