package com.learning.portfolio.stock.tracker.cache;

public enum CacheIntervals {
    FIFTEEN_MINUTE_INTERVAL(15*60*1000),
    THIRTY_MINUTE_INTERVAL(30*60*1000),
    ONE_HOUR_INTERVAL(60*60*1000),
    ONE_DAY_INTERVAL(24*60*60*1000);

    private long timeInMillis;

    CacheIntervals(long timeInMillis) {
        this.timeInMillis = timeInMillis;
    }

    public long getTimeInMillis() {
        return timeInMillis;
    }
}
