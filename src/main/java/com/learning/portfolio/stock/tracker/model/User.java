package com.learning.portfolio.stock.tracker.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

public class User {

    @NotEmpty(message = "username must not be empty")
    private String userName;

    @NotEmpty(message = "first name must not be empty")
    private String firstName;

    @NotEmpty(message = "last name must not be empty")
    private String lastName;

    @Email(message = "emailId is not valid")
    @NotEmpty(message = "emailId must not be empty")
    private String emailID;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }
}
