package com.learning.portfolio.stock.tracker.repository;

import com.learning.portfolio.stock.tracker.entity.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Component;

@RepositoryRestResource(collectionResourceRel = "Stock",path = "Stock")
@Component
public interface StocksRepository extends JpaRepository<Stock,String> {

}
