package com.learning.portfolio.stock.tracker.model;

import com.learning.portfolio.stock.tracker.entity.TransactionType;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class StockSingleTransactionData {
    public StockSingleTransactionData(){}
    @NotEmpty(message = "stockName cannot be empty/null")
    private String stockName;
    @NotNull(message = "Buy price can't be null")
    private double price;
    @NotNull(message = "Quantity can't be null")
    private int quantity;
    private String notes;
    @NotNull(message = "Date of purchase can't be null")
    private Date transactionDate;

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getNotes() {
        return notes;
    }
    //TODO add text trimming limit
    public void setNotes(String notes) {
        this.notes = notes;
    }
}
