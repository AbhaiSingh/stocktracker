package com.learning.portfolio.stock.tracker.repository;

import com.learning.portfolio.stock.tracker.entity.StockLedger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Component;

import java.util.UUID;

@RepositoryRestResource(collectionResourceRel = "StockLedger", path = "StockLedger")
@Component
public interface StockLedgerRepository extends JpaRepository<StockLedger, UUID> {

}
