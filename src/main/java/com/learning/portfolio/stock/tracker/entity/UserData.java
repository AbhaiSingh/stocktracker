package com.learning.portfolio.stock.tracker.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "user_data")
public class UserData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "username")
    private String username;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "surname")
    private String surname;

    @Column(name ="email")
    private String email;

    //Check if helper method is needed to manage bidirectional persistence
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<StockLedger> stockTransactions = new ArrayList<>();

    //Check if helper method is needed to manage bidirectional persistence
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<StockPortfolio> stockPortfolios = new ArrayList<>();

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<StockLedger> getStockTransactions() {
        return stockTransactions;
    }

    /**
     * Warning : Bidirectional persistence is not managed currently
     * @param stockTransactions
     */
    public void setStockTransactions(List<StockLedger> stockTransactions) {
        this.stockTransactions = stockTransactions;
    }

    public List<StockPortfolio> getStockPortfolios() {
        return stockPortfolios;
    }

    /**
     * Warning : Bidirectional persistence is not managed currently
     * @param stockPortfolios
     */
    public void setStockPortfolios(List<StockPortfolio> stockPortfolios) {
        this.stockPortfolios = stockPortfolios;
    }
}
