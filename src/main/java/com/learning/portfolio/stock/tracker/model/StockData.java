package com.learning.portfolio.stock.tracker.model;

public class StockData {

    public StockData() {
    }

    private String stockName;
    private double buyPrice;
    private double currentPrice;
    private int quantity;
    private double profitLoss;
    private double profitLossPercent;
    private String notes;

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public double getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(double buyPrice) {
        this.buyPrice = buyPrice;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getProfitLoss() {
        return profitLoss;
    }

    public void setProfitLoss(double profitLoss) {
        this.profitLoss = profitLoss;
    }

    public double getProfitLossPercent() {
        return profitLossPercent;
    }

    public void setProfitLossPercent(double profitLossPercent) {
        this.profitLossPercent = profitLossPercent;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public String toString() {
        return "StockData{" +
                "stockName='" + stockName + '\'' +
                ", buyPrice=" + buyPrice +
                ", currentPrice=" + currentPrice +
                ", quantity=" + quantity +
                ", profitLoss=" + profitLoss +
                ", profitLossPercent=" + profitLossPercent +
                '}';
    }
}
