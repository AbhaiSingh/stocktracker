package com.learning.portfolio.stock.tracker.entity;

public enum TransactionType {
    BUY,
    SELL
}
