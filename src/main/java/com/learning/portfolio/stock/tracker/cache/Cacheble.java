package com.learning.portfolio.stock.tracker.cache;

public interface Cacheble {

    Object getIdentifier();

    boolean isExpired();
}
