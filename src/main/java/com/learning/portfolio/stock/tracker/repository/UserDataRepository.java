package com.learning.portfolio.stock.tracker.repository;

import com.learning.portfolio.stock.tracker.entity.UserData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@RepositoryRestResource(collectionResourceRel = "UserData", path = "UserData")
@Component
public interface UserDataRepository extends JpaRepository<UserData, UUID> {

    @Query(value = "Select * from user_data where username= ?1", nativeQuery = true)
    Optional<UserData> findByUsername(String username);


}
