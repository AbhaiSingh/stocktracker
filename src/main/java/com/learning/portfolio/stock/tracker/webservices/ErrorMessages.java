package com.learning.portfolio.stock.tracker.webservices;

public enum ErrorMessages {

    USER_NOT_PRESENT("Invalid username","User %s doesn't exist");

    private String errorCause;
    private String errorDetails;

    ErrorMessages(String errorCause, String errorDetails) {
        this.errorCause = errorCause;
        this.errorDetails = errorDetails;
    }

    public String getErrorCause() {
        return errorCause;
    }

    public String getErrorDetails() {
        return errorDetails;
    }
}
