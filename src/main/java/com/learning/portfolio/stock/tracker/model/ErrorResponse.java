package com.learning.portfolio.stock.tracker.model;

import java.util.ArrayList;
import java.util.List;

public class ErrorResponse {

    private String errorMessage;
    private List<String> errorDetails;

    public ErrorResponse(String errorMessage, List<String> errorDetails) {
        this.errorMessage = errorMessage;
        this.errorDetails = errorDetails;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public List<String> getErrorDetails() {
        return errorDetails.isEmpty() ? new ArrayList<>() : errorDetails;
    }

}
