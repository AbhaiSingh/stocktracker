package com.learning.portfolio.stock.tracker.model;

import com.learning.portfolio.stock.tracker.entity.TransactionType;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

public class StockPurchseData {

    @NotEmpty(message = "stockName cannot be empty/null")
    private String stockName;
    @NotEmpty(message = "buyPrice cannot be empty/null")
    private double buyPrice;
    @NotEmpty(message = "quantity cannot be empty/null")
    private int quantity;
    @NotEmpty(message = "dateOfPurchase cannot be empty/null")
    private Date dateOfPurchase;
    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public double getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(double buyPrice) {
        this.buyPrice = buyPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getDateOfPurchase() {
        return dateOfPurchase;
    }

    public void setDateOfPurchase(Date dateOfPurchase) {
        this.dateOfPurchase = dateOfPurchase;
    }

}
