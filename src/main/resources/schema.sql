
create table if not exists Stocks (
  ISIN varchar not null,
  SYMBOL varchar(45) not null,
  NAME varchar(100) null,
  LAST_UPDATED date,
  primary key (ISIN)
  );

create TABLE if not exists user_data(
    id uuid NOT NULL,
    username varchar(15) NOT NULL,
    email varchar(50) NOT NULL,
    first_name varchar(30) NOT NULL,
    surname varchar(50),
    PRIMARY KEY (id)
);



create TABLE if not exists public.stock_portfolio(
    id uuid NOT NULL,
    user_id uuid NOT NULL,
    symbol varchar(20) NOT NULL,
    buy_price decimal NOT NULL,
    current_price decimal,
    quantity int NOT NULL,
    notes varchar(200),
    updated_on date NOT NULL,
    CONSTRAINT stock_portfolio_pkey PRIMARY KEY (id),
    CONSTRAINT user_id FOREIGN KEY (user_id)
        REFERENCES public.user_data(id)
);

create TABLE if not exists stock_ledger(
    id uuid NOT NULL,
    user_id uuid NOT NULL,
    symbol varchar(20) NOT NULL,
    type varchar(10) NOT NULL,
    price varchar NOT NULL,
    quantity int NOT NULL,
    notes varchar(200),
    date date NOT NULL,
    CONSTRAINT stock_ledger_pkey PRIMARY KEY (id),
    CONSTRAINT stock_ledger_fkey FOREIGN KEY (user_id)
        REFERENCES user_data(id)
);

  create table if not exists tradingholidays(
    MARKET_SEGMENT varchar(11) not null,
    DATE DATE NOT NULL,
    DESCRIPTION varchar(255) null,
    primary key (DATE)
    );