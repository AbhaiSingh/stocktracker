package com.learning.portfolio.stock.tracker.services;

import com.learning.portfolio.stock.tracker.StockTrackerApplicationTests;
import com.learning.portfolio.stock.tracker.dao.services.UserDataServices;
import com.learning.portfolio.stock.tracker.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class TestUserDataServices extends StockTrackerApplicationTests {

    @Autowired
    UserDataServices userDataServices;

    @Test
    public void sunnyDayPersistUser(){
        createUser("Admin", "Admin1@gmail.com", "Crappy", "Craperson");
        Assert.assertTrue(userDataServices.isUsernamePresent("Admin"));
    }

    @Test
    public void testUsernameNotPesent(){
        Assert.assertFalse(userDataServices.isUsernamePresent("Admin"));
    }

    @Test
    public void deleteUser(){
        User user = createUser("QA", "QA@gmail.com", "Donald", "Duck");
        Assert.assertTrue(userDataServices.isUsernamePresent(user.getUserName()));
        userDataServices.deleteUser(user.getUserName());
        Assert.assertFalse(userDataServices.isUsernamePresent(user.getUserName()));
    }

    private User createUser(String username, String email, String fname, String sname) {
        User input = new User();
        input.setUserName(username);
        input.setEmailID(email);
        input.setFirstName(fname);
        input.setLastName(sname);
        userDataServices.createUser(input);

        return input;
    }

}
