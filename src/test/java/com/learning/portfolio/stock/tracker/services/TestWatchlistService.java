package com.learning.portfolio.stock.tracker.services;

import com.learning.portfolio.stock.tracker.StockTrackerApplicationTests;
import com.learning.portfolio.stock.tracker.dao.services.StockPortfolioService;
import com.learning.portfolio.stock.tracker.dao.services.UserDataServices;
import com.learning.portfolio.stock.tracker.model.StockSingleTransactionData;
import com.learning.portfolio.stock.tracker.model.UpdateWatchListInput;
import com.learning.portfolio.stock.tracker.model.User;
import com.learning.portfolio.stock.tracker.model.WatchlistPerformanceResponse;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.junit.Assert.assertEquals;

public class TestWatchlistService extends StockTrackerApplicationTests {

    @Autowired
    private StockPortfolioService stockPortfolioService;
    @Autowired
    private UserDataServices userDataServices;


    @Test
    public void testbasic(){
        User user = createUser("QA", "QA@gmail.com", "Donald", "Duck");
        UpdateWatchListInput watchListInput = prepareWatchlistInput(user,"BERGEPAINT",353,20);
        stockPortfolioService.addToPortfolio(watchListInput);
        UpdateWatchListInput watchListInput2 = prepareWatchlistInput(user,"BERGEPAINT",353,10);
        stockPortfolioService.addToPortfolio(watchListInput2);
        assertEquals(1, stockPortfolioService.getStockPortfolio(user.getUserName()).size());
        WatchlistPerformanceResponse performanceResponse = stockPortfolioService.buildWatchlistPerformance(user.getUserName());
        System.out.println(performanceResponse);
    }

    private UpdateWatchListInput prepareWatchlistInput(User user, String stock, double price, int quantity) {
        UpdateWatchListInput watchListInput = new UpdateWatchListInput();
        watchListInput.setUserName(user.getUserName());
        StockSingleTransactionData singleTransactionData = new StockSingleTransactionData();
        singleTransactionData.setStockName(stock);
        singleTransactionData.setPrice(price);
        singleTransactionData.setQuantity(quantity);
        singleTransactionData.setNotes("Sell at 380");
        singleTransactionData.setTransactionDate(new Date());
        watchListInput.setStock(singleTransactionData);
        return watchListInput;
    }

    private User createUser(String username, String email, String fname, String sname) {
        User input = new User();
        input.setUserName(username);
        input.setEmailID(email);
        input.setFirstName(fname);
        input.setLastName(sname);
        userDataServices.createUser(input);

        return input;
    }
}
