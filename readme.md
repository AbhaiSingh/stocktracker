## ****Stock tracker application****

#### ****About****
This is a spring-boot based backend stock tracker application which can be used for tracking stock portfolio.
Have created a separate java-fx based UI application named portfolio tracker which consumes stock tracker api and performes basic stock purchase and sell operations.
 
#### ****Technology****
1. H2 Database (store the information on file instead of in memory behaviour)
2. Restful services
3. spring-boot, spring-jpa, spring-batch

#### ****Data Feed****
Stock information (name, symbol, current price) is fetched through:
1. Scraping NSE website (bhavcopy data) `https://www.nseindia.com/`
2. Alphavantage webservices `https://www.alphavantage.co/`
  